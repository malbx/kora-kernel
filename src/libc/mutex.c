/*
 *      This file is part of the KoraOS project.
 *  Copyright (C) 2018  <Fabien Bavent>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   - - - - - - - - - - - - - - -
 */
#include <threads.h>
#include <kernel/task.h>
#include <kora/splock.h>
#include <errno.h>
#include <limits.h>


thrd_t thrd_current(void)
{
    return NULL;
}
int thrd_equals(thrd_t l, thrd_t r)
{
    return l == r;
}

struct _US_MUTEX {
    atomic_t counter;
    int flags;
    thrd_t thread;
    splock_t splock;
    emitter_t emitter;
};

int futex_wait(struct _US_MUTEX *mutex, const struct timespec *ts)
{
    time64_t until = 0;
    for (;;) {
        splock_lock(&mutex->splock);
        if (atomic_cmpxchg(&mutex->counter, 0, 1) == 0) {
            // Don't wait, you got it!
            splock_unlock(&mutex->splock);
            return 0;
        }
        int res = async_wait(&mutex->splock, &mutex->emitter, until - time64());
        if (res == EAGAIN) {
            splock_unlock(&mutex->splock);
            return -1;
        }
    }
}

void futex_raise(struct _US_MUTEX *mutex)
{
    splock_lock(&mutex->splock);
    async_raise(&mutex->emitter, 0);
    splock_unlock(&mutex->splock);
}

/* Creates a mutex */
int mtx_init(mtx_t *mutex, int flags)
{
    // TODO - Use shm in case of mtx_shared
    struct _US_MUTEX *ptr = malloc(sizeof(struct _US_MUTEX));
    ptr->counter = 0;
    ptr->flags = flags;
    ptr->thread = 0;
    splock_lock(&ptr->splock);
    *mutex = ptr;
    return 0;
}

/* Blocks locks a mutex */
int mtx_lock(mtx_t *mutex)
{
    struct timespec ts;
    ts.tv_sec = LONG_MAX;
    return mtx_timedlock(mutex, &ts);
}

/* Blocks until locks a mutex or times out */
int mtx_timedlock(mtx_t *mutex, const struct timespec *until)
{
    struct _US_MUTEX *ptr = *mutex;
    if (ptr->counter > 0 && (ptr->flags & mtx_recursive) && thrd_equals(ptr->thread, thrd_current()))
        return 0;

    if (atomic_xadd(&ptr->counter, 1) == 0) {
        ptr->thread = thrd_current();
        return 0;
    }

    if (futex_wait(ptr, until) == 0) {
        ptr->thread = thrd_current();
        return 0;
    }
    return -1;
}

/* Locks a mutex or returns without blocking if already locked */
int mtx_trylock(mtx_t *mutex)
{
    struct _US_MUTEX *ptr = *mutex;
    if (ptr->counter > 0 && (ptr->flags & mtx_recursive) && thrd_equals(ptr->thread, thrd_current()))
        return 0;

    if (atomic_cmpxchg(&ptr->counter, 0, 1) == 0) {
        ptr->thread = thrd_current();
        return 0;
    }

    return 1;
}

/* Unlocks a mutex */
int mtx_unlock(mtx_t *mutex)
{
    struct _US_MUTEX *ptr = *mutex;
    int prev = atomic_xchg(&ptr->counter, 0);
    if (prev > 1)
        futex_raise(ptr);
    return 0;
}

/* Destroys a mutex */
void mtx_destroy(mtx_t *mutex)
{
    struct _US_MUTEX *ptr = *mutex;
    free(ptr);
}

