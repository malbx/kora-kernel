/*
 *      This file is part of the KoraOS project.
 *  Copyright (C) 2018  <Fabien Bavent>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   - - - - - - - - - - - - - - -
 */
case 0x0033:
return "Paradyne Corp.";
case 0x003D:
return "master";
case 0x0070:
return "Hauppauge Computer Works Inc.";
case 0x0100:
return "USBPDO-8";
case 0x0123:
return "General Dynamics";
case 0x0315:
return "SK - Electronics Co., Ltd.";
case 0x0402:
return "Acer aspire one";
case 0x046D:
return "Logitech Inc.";
case 0x0483:
return "UPEK";
case 0x04A9:
return "Canon";
case 0x04B3:
return "IBM";
case 0x04D9:
return "Filco";
case 0x04F2:
return "Chicony Electronics Co. ";
case 0x051D:
return "ACPI\\VEN_INT&DEV_33A0";
case 0x0529:
return "Aladdin E-Token";
case 0x0553:
return "Aiptek USA";
case 0x058f:
return "Alcor Micro Corp.";
case 0x0590:
return "Omron Corp";
case 0x05ac:
return "Apple Inc.";
case 0x05E1:
return "D-MAX";
case 0x064e:
return "SUYIN Corporation";
case 0x067B:
return "Prolific Technology Inc.";
case 0x06FE:
return "Acresso Software Inc.";
case 0x0711:
return "SIIG, Inc.";
case 0x093a:
return "KYE Systems Corp. / Pixart Imaging";
case 0x096E:
return "USB Rockey dongle from Feitain ";
case 0x0A5C:
return "Broadcom Corporation";
case 0x0A89:
return "BREA Technologies Inc.";
case 0x0A92:
return "Egosys, Inc.";
case 0x0AC8:
return "ASUS ";
case 0x0b05:
return "Toshiba Bluetooth RFBUS, RFCOM, RFHID";
case 0x0c45:
return "Microdia Ltd.";
case 0x0cf3:
return "TP-Link";
case 0x0D2E:
return "Feedback Instruments Ltd.";
case 0x0D8C:
return "C-Media Electronics, Inc.";
case 0x0DF6:
return "Sitecom";
case 0x0E11:
return "Compaq Computer Corp.";
case 0x0E8D:
return "MediaTek Inc.";
case 0x1000:
return "LSI Logic";
case 0x1001:
return "Kolter Electronic - Germany";
case 0x1002:
return "Advanced Micro Devices, Inc.";
case 0x1003:
return "ULSI";
case 0x1004:
return "VLSI Technology";
case 0x1006:
return "Reply Group";
case 0x1007:
return "Netframe Systems Inc.";
case 0x1008:
return "Epson";
case 0x100A:
return "Ã‚as Ltd. de Phoenix del Âƒ de TecnologÃƒ";
case 0x100B:
return "National Semiconductors";
case 0x100C:
return "Tseng Labs";
case 0x100D:
return "AST Research";
case 0x100E:
return "Weitek";
case 0x1010:
return "Video Logic Ltd.";
case 0x1011:
return "Digital Equipment Corporation";
case 0x1012:
return "Micronics Computers Inc.";
case 0x1013:
return "Cirrus Logic";
case 0x1014:
return "International Business Machines Corp.";
case 0x1016:
return "Fujitsu ICL Computers";
case 0x1017:
return "Spea Software AG";
case 0x1018:
return "Unisys Systems";
case 0x1019:
return "Elitegroup Computer System";
case 0x101A:
return "NCR Corporation";
case 0x101B:
return "Vitesse Semiconductor";
case 0x101E:
return "American Megatrends Inc.";
case 0x101F:
return "PictureTel Corp.";
case 0x1020:
return "Hitachi Computer Electronics";
case 0x1021:
return "Oki Electric Industry";
case 0x1022:
return "Advanced Micro Devices";
case 0x1023:
return "TRIDENT MICRO";
case 0x1025:
return "Acer Incorporated";
case 0x1028:
return "Dell Inc.";
case 0x102A:
return "LSI Logic Headland Division";
case 0x102B:
return "Matrox Electronic Systems Ltd.";
case 0x102C:
return "Asiliant (Chips And Technologies)";
case 0x102D:
return "Wyse Technology";
case 0x102E:
return "Olivetti Advanced Technology";
case 0x102F:
return "Toshiba America";
case 0x1030:
return "TMC Research";
case 0x1031:
return "miro Computer Products AG";
case 0x1033:
return "NEC Electronics";
case 0x1034:
return "Burndy Corporation";
case 0x1036:
return "Future Domain";
case 0x1037:
return "Hitachi Micro Systems Inc";
case 0x1038:
return "AMP Incorporated";
case 0x1039:
return "Silicon Integrated Systems";
case 0x103A:
return "Seiko Epson Corporation";
case 0x103B:
return "Tatung Corp. Of America";
case 0x103C:
return "Hewlett-Packard";
case 0x103E:
return "Solliday Engineering";
case 0x103F:
return "Logic Modeling";
case 0x1041:
return "Computrend";
case 0x1043:
return "Asustek Computer Inc.";
case 0x1044:
return "Distributed Processing Tech";
case 0x1045:
return "OPTi Inc.";
case 0x1046:
return "IPC Corporation LTD";
case 0x1047:
return "Genoa Systems Corp.";
case 0x1048:
return "ELSA GmbH";
case 0x1049:
return "Fountain Technology";
case 0x104A:
return "STMicroelectronics";
case 0x104B:
return "Mylex / Buslogic";
case 0x104C:
return "Texas Instruments";
case 0x104D:
return "Sony Corporation";
case 0x104E:
return "Oak Technology";
case 0x104F:
return "Co-Time Computer Ltd.";
case 0x1050:
return "Winbond Electronics Corp.";
case 0x1051:
return "Anigma Corp.";
case 0x1053:
return "Young Micro Systems";
case 0x1054:
return "Hitachi Ltd";
case 0x1055:
return "Standard Microsystems Corp.";
case 0x1056:
return "ICL";
case 0x1057:
return "Motorola";
case 0x1058:
return "Electronics & Telecommunication Res";
case 0x1059:
return "Kontron Canada";
case 0x105A:
return "Promise Technology";
case 0x105B:
return "Mobham chip";
case 0x105C:
return "Wipro Infotech Limited";
case 0x105D:
return "Number Nine Visual Technology";
case 0x105E:
return "Vtech Engineering Canada Ltd.";
case 0x105F:
return "Infotronic America Inc.";
case 0x1060:
return "United Microelectronics";
case 0x1061:
return "8x8 Inc.";
case 0x1062:
return "Maspar Computer Corp.";
case 0x1063:
return "Ocean Office Automation";
case 0x1064:
return "Alcatel Cit";
case 0x1065:
return "Texas Microsystems";
case 0x1066:
return "Picopower Technology";
case 0x1067:
return "Mitsubishi Electronics";
case 0x1068:
return "Diversified Technology";
case 0x106A:
return "Aten Research Inc.";
case 0x106B:
return "Apple Inc.";
case 0x106C:
return "Hyundai Electronics America";
case 0x106D:
return "Sequent Computer Systems";
case 0x106E:
return "DFI Inc.";
case 0x106F:
return "City Gate Development LTD";
case 0x1070:
return "Daewoo Telecom Ltd.";
case 0x1071:
return "Mitac";
case 0x1072:
return "GIT Co. Ltd.";
case 0x1073:
return "Yamaha Corporation";
case 0x1074:
return "Nexgen Microsystems";
case 0x1075:
return "Advanced Integration Research";
case 0x1077:
return "QLogic Corporation";
case 0x1078:
return "Cyrix Corporation";
case 0x1079:
return "I-Bus";
case 0x107A:
return "Networth controls";
case 0x107B:
return "Gateway 2000";
case 0x107C:
return "Goldstar Co. Ltd.";
case 0x107D:
return "Leadtek Research";
case 0x107E:
return "Testernec";
case 0x107F:
return "Data Technology Corporation";
case 0x1080:
return "Cypress Semiconductor";
case 0x1081:
return "Radius Inc.";
case 0x1082:
return "EFA Corporation Of America";
case 0x1083:
return "Forex Computer Corporation";
case 0x1084:
return "Parador";
case 0x1085:
return "Tulip Computers Int'l BV";
case 0x1086:
return "J. Bond Computer Systems";
case 0x1087:
return "Cache Computer";
case 0x1088:
return "Microcomputer Systems (M) Son";
case 0x1089:
return "Data General Corporation";
case 0x108A:
return "SBS Operations";
case 0x108C:
return "Oakleigh Systems Inc.";
case 0x108D:
return "Olicom";
case 0x108E:
return "Sun Microsystems";
case 0x108F:
return "Systemsoft Corporation";
case 0x1090:
return "Encore Computer Corporation";
case 0x1091:
return "Intergraph Corporation";
case 0x1092:
return "Diamond Computer Systems";
case 0x1093:
return "National Instruments";
case 0x1094:
return "Apostolos";
case 0x1095:
return "Silicon Image, Inc.";
case 0x1096:
return "Alacron";
case 0x1097:
return "Appian Graphics";
case 0x1098:
return "Quantum Designs Ltd.";
case 0x1099:
return "Samsung Electronics Co. Ltd.";
case 0x109A:
return "Packard Bell";
case 0x109B:
return "Gemlight Computer Ltd.";
case 0x109C:
return "Megachips Corporation";
case 0x109D:
return "Zida Technologies Ltd.";
case 0x109E:
return "Brooktree Corporation";
case 0x109F:
return "Trigem Computer Inc.";
case 0x10A0:
return "Meidensha Corporation";
case 0x10A1:
return "Juko Electronics Inc. Ltd.";
case 0x10A2:
return "Quantum Corporation";
case 0x10A3:
return "Everex Systems Inc.";
case 0x10A4:
return "Globe Manufacturing Sales";
case 0x10A5:
return "Racal Interlan";
case 0x10A8:
return "Sierra Semiconductor";
case 0x10A9:
return "Silicon Graphics";
case 0x10AB:
return "Digicom";
case 0x10AC:
return "Honeywell IASD";
case 0x10AD:
return "Winbond Systems Labs";
case 0x10AE:
return "Cornerstone Technology";
case 0x10AF:
return "Micro Computer Systems Inc.";
case 0x10B0:
return "Gainward GmbH ";
case 0x10B1:
return "Cabletron Systems Inc.";
case 0x10B2:
return "Raytheon Company";
case 0x10B3:
return "Databook Inc.";
case 0x10B4:
return "STB Systems";
case 0x10B5:
return "PLX Technology Inc.";
case 0x10B6:
return "Madge Networks";
case 0x10B7:
return "3Com Corporation";
case 0x10B8:
return "Standard Microsystems Corporation";
case 0x10B9:
return "Ali Corporation";
case 0x10BA:
return "Mitsubishi Electronics Corp.";
case 0x10BB:
return "Dapha Electronics Corporation";
case 0x10BC:
return "Advanced Logic Research Inc.";
case 0x10BD:
return "Surecom Technology";
case 0x10BE:
return "Tsenglabs International Corp.";
case 0x10BF:
return "MOST Corp.";
case 0x10C0:
return "Boca Research Inc.";
case 0x10C1:
return "ICM Corp. Ltd.";
case 0x10C2:
return "Auspex Systems Inc.";
case 0x10C3:
return "Samsung Semiconductors";
case 0x10C4:
return "Award Software Int'l Inc.";
case 0x10C5:
return "Xerox Corporation";
case 0x10C6:
return "Rambus Inc.";
case 0x10C8:
return "Neomagic Corporation";
case 0x10C9:
return "Dataexpert Corporation";
case 0x10CA:
return "Fujitsu Siemens";
case 0x10CB:
return "Omron Corporation";
case 0x10CD:
return "Advanced System Products";
case 0x10CF:
return "Fujitsu Ltd.";
case 0x10D1:
return "Future+ Systems";
case 0x10D2:
return "Molex Incorporated";
case 0x10D3:
return "Jabil Circuit Inc.";
case 0x10D4:
return "Hualon Microelectronics";
case 0x10D5:
return "Autologic Inc.";
case 0x10D6:
return "Wilson .co .ltd";
case 0x10D7:
return "BCM Advanced Research";
case 0x10D8:
return "Advanced Peripherals Labs";
case 0x10D9:
return "Macronix International Co. Ltd.";
case 0x10DB:
return "Rohm Research";
case 0x10DC:
return "CERN-European Lab. for Particle Physics";
case 0x10DD:
return "Evans & Sutherland";
case 0x10DE:
return "NVIDIA";
case 0x10DF:
return "Emulex Corporation";
case 0x10E1:
return "Tekram Technology Corp. Ltd.";
case 0x10E2:
return "Aptix Corporation";
case 0x10E3:
return "Tundra Semiconductor Corp.";
case 0x10E4:
return "Tandem Computers";
case 0x10E5:
return "Micro Industries Corporation";
case 0x10E6:
return "Gainbery Computer Products Inc.";
case 0x10E7:
return "Vadem";
case 0x10E8:
return "Applied Micro Circuits Corp.";
case 0x10E9:
return "Alps Electronic Corp. Ltd.";
case 0x10EA:
return "Tvia, Inc.";
case 0x10EB:
return "Artist Graphics";
case 0x10EC:
return "Realtek Semiconductor Corp.";
case 0x10ED:
return "Ascii Corporation";
case 0x10EE:
return "Xilinx Corporation";
case 0x10EF:
return "Racore Computer Products";
case 0x10F0:
return "Curtiss-Wright Controls Embedded Computing";
case 0x10F1:
return "Tyan Computer";
case 0x10F2:
return "Achme Computer Inc. - GONE !!!!";
case 0x10F3:
return "Alaris Inc.";
case 0x10F4:
return "S-Mos Systems";
case 0x10F5:
return "NKK Corporation";
case 0x10F6:
return "Creative Electronic Systems SA";
case 0x10F7:
return "Matsushita Electric Industrial Corp.";
case 0x10F8:
return "Altos India Ltd.";
case 0x10F9:
return "PC Direct";
case 0x10FA:
return "Truevision";
case 0x10FB:
return "Thesys Microelectronic's";
case 0x10FC:
return "I-O Data Device Inc.";
case 0x10FD:
return "Soyo Technology Corp. Ltd.";
case 0x10FE:
return "Fast Electronic GmbH";
case 0x10FF:
return "Ncube";
case 0x1100:
return "Jazz Multimedia";
case 0x1101:
return "Initio Corporation";
case 0x1102:
return "Creative Technology LTD.";
case 0x1103:
return "HighPoint Technologies, Inc.";
case 0x1104:
return "Rasterops";
case 0x1105:
return "Sigma Designs Inc.";
case 0x1106:
return "VIA Technologies, Inc.";
case 0x1107:
return "Stratus Computer";
case 0x1108:
return "Proteon Inc.";
case 0x1109:
return "Adaptec/Cogent Data Technologies";
case 0x110A:
return "Siemens AG";
case 0x110B:
return "Chromatic Research Inc";
case 0x110C:
return "Mini-Max Technology Inc.";
case 0x110D:
return "ZNYX Corporation";
case 0x110E:
return "CPU Technology";
case 0x110F:
return "Ross Technology";
case 0x1112:
return "Osicom Technologies Inc.";
case 0x1113:
return "Accton Technology Corporation";
case 0x1114:
return "Atmel Corp.";
case 0x1116:
return "Data Translation, Inc.";
case 0x1117:
return "Datacube Inc.";
case 0x1118:
return "Berg Electronics";
case 0x1119:
return "ICP vortex Computersysteme GmbH";
case 0x111A:
return "Efficent Networks";
case 0x111C:
return "Tricord Systems Inc.";
case 0x111D:
return "Integrated Device Technology Inc.";
case 0x111F:
return "Precision Digital Images";
case 0x1120:
return "EMC Corp.";
case 0x1121:
return "Zilog";
case 0x1123:
return "Excellent Design Inc.";
case 0x1124:
return "Leutron Vision AG";
case 0x1125:
return "Eurocore/Vigra";
case 0x1127:
return "FORE Systems";
case 0x1129:
return "Firmworks";
case 0x112A:
return "Hermes Electronics Co. Ltd.";
case 0x112C:
return "Zenith Data Systems";
case 0x112D:
return "Ravicad";
case 0x112E:
return "Infomedia";
case 0x1130:
return "Computervision";
case 0x1131:
return "NXP Semiconductors N.V.";
case 0x1132:
return "Mitel Corp.";
case 0x1133:
return "Eicon Networks Corporation";
case 0x1134:
return "Mercury Computer Systems Inc.";
case 0x1135:
return "Fuji Xerox Co Ltd";
case 0x1136:
return "Momentum Data Systems";
case 0x1137:
return "Cisco Systems Inc";
case 0x1138:
return "Ziatech Corporation";
case 0x1139:
return "Dynamic Pictures Inc";
case 0x113A:
return "FWB Inc";
case 0x113B:
return "Network Computing Devices";
case 0x113C:
return "Cyclone Microsystems Inc.";
case 0x113D:
return "Leading Edge Products Inc";
case 0x113E:
return "Sanyo Electric Co";
case 0x113F:
return "Equinox Systems";
case 0x1140:
return "Intervoice Inc";
case 0x1141:
return "Crest Microsystem Inc";
case 0x1142:
return "Alliance Semiconductor";
case 0x1143:
return "Netpower Inc";
case 0x1144:
return "Cincinnati Milacron";
case 0x1145:
return "Workbit Corp";
case 0x1146:
return "Force Computers";
case 0x1147:
return "Interface Corp";
case 0x1148:
return "Marvell Semiconductor Germany GmbH";
case 0x1149:
return "Win System Corporation";
case 0x114A:
return "VMIC";
case 0x114B:
return "Canopus corporation";
case 0x114C:
return "Annabooks";
case 0x114D:
return "IC Corporation";
case 0x114E:
return "Nikon Systems Inc";
case 0x114F:
return "Digi International";
case 0x1150:
return "Thinking Machines Corporation";
case 0x1151:
return "JAE Electronics Inc.";
case 0x1153:
return "Land Win Electronic Corp";
case 0x1154:
return "Melco Inc";
case 0x1155:
return "Pine Technology Ltd";
case 0x1156:
return "Periscope Engineering";
case 0x1157:
return "Avsys Corporation";
case 0x1158:
return "Voarx R&D Inc";
case 0x1159:
return "Mutech";
case 0x115A:
return "Harlequin Ltd";
case 0x115B:
return "Parallax Graphics";
case 0x115C:
return "Photron Ltd.";
case 0x115D:
return "Xircom";
case 0x115E:
return "Peer Protocols Inc";
case 0x115F:
return "Maxtor Corporation";
case 0x1160:
return "Megasoft Inc";
case 0x1161:
return "PFU Ltd";
case 0x1162:
return "OA Laboratory Co Ltd";
case 0x1163:
return "mohamed alsherif";
case 0x1164:
return "Advanced Peripherals Tech";
case 0x1165:
return "Imagraph Corporation";
case 0x1166:
return "Broadcom / ServerWorks";
case 0x1167:
return "Mutoh Industries Inc";
case 0x1168:
return "Thine Electronics Inc";
case 0x1169:
return "Centre f/Dev. of Adv. Computing";
case 0x116A:
return "Luminex Software, Inc";
case 0x116B:
return "Connectware Inc";
case 0x116C:
return "Intelligent Resources";
case 0x116E:
return "Electronics for Imaging";
case 0x1170:
return "Inventec Corporation";
case 0x1172:
return "Altera Corporation";
case 0x1173:
return "Adobe Systems";
case 0x1174:
return "Bridgeport Machines";
case 0x1175:
return "Mitron Computer Inc.";
case 0x1176:
return "SBE";
case 0x1177:
return "Silicon Engineering";
case 0x1178:
return "Alfa Inc";
case 0x1179:
return "Toshiba corporation";
case 0x117A:
return "A-Trend Technology";
case 0x117B:
return "LG (Lucky Goldstar) Electronics Inc.";
case 0x117C:
return "Atto Technology";
case 0x117D:
return "Becton & Dickinson";
case 0x117E:
return "T/R Systems";
case 0x117F:
return "Integrated Circuit Systems";
case 0x1180:
return "RicohCompany,Ltd.";
case 0x1183:
return "Fujikura Ltd";
case 0x1184:
return "Forks Inc";
case 0x1185:
return "Dataworld";
case 0x1186:
return "D-Link System Inc";
case 0x1187:
return "Philips Healthcare";
case 0x1188:
return "Shima Seiki Manufacturing Ltd.";
case 0x1189:
return "Matsushita Electronics";
case 0x118A:
return "Hilevel Technology";
case 0x118B:
return "Hypertec Pty Ltd";
case 0x118C:
return "Corollary Inc";
case 0x118D:
return "BitFlow Inc";
case 0x118E:
return "Hermstedt AG";
case 0x118F:
return "Green Logic";
case 0x1190:
return "Tripace";
case 0x1191:
return "Acard Technology Corp.";
case 0x1192:
return "Densan Co. Ltd";
case 0x1194:
return "Toucan Technology";
case 0x1195:
return "Ratoc System Inc";
case 0x1196:
return "Hytec Electronics Ltd";
case 0x1197:
return "Gage Applied Technologies";
case 0x1198:
return "Lambda Systems Inc";
case 0x1199:
return "Attachmate Corp.";
case 0x119A:
return "Mind/Share Inc.";
case 0x119B:
return "Omega Micro Inc.";
case 0x119C:
return "Information Technology Inst.";
case 0x119D:
return "Bug Sapporo Japan";
case 0x119E:
return "Fujitsu Microelectronics Ltd.";
case 0x119F:
return "Bull Hn Information Systems";
case 0x11A1:
return "Hamamatsu Photonics K.K.";
case 0x11A2:
return "Sierra Research and Technology";
case 0x11A3:
return "Deuretzbacher GmbH & Co. Eng. KG";
case 0x11A4:
return "Barco";
case 0x11A5:
return "MicroUnity Systems Engineering Inc.";
case 0x11A6:
return "Pure Data";
case 0x11A7:
return "Power Computing Corp.";
case 0x11A8:
return "Systech Corp.";
case 0x11A9:
return "InnoSys Inc.";
case 0x11AA:
return "Actel";
case 0x11AB:
return "Marvell Semiconductor";
case 0x11AC:
return "Canon Information Systems";
case 0x11AD:
return "Lite-On Technology Corp.";
case 0x11AE:
return "Scitex Corporation Ltd";
case 0x11AF:
return "Avid Technology, Inc.";
case 0x11B0:
return "Quicklogic Corp";
case 0x11B1:
return "Apricot Computers";
case 0x11B2:
return "Eastman Kodak";
case 0x11B3:
return "Barr Systems Inc.";
case 0x11B4:
return "Leitch Technology International";
case 0x11B5:
return "Radstone Technology Ltd.";
case 0x11B6:
return "United Video Corp";
case 0x11B7:
return "Motorola";
case 0x11B8:
return "Xpoint Technologies Inc";
case 0x11B9:
return "Pathlight Technology Inc.";
case 0x11BA:
return "Videotron Corp";
case 0x11BB:
return "Pyramid Technology";
case 0x11BC:
return "Network Peripherals Inc";
case 0x11BD:
return "Pinnacle system";
case 0x11BE:
return "International Microcircuits Inc";
case 0x11BF:
return "Astrodesign Inc.";
case 0x11C1:
return "LSI Corporation";
case 0x11C2:
return "Sand Microelectronics";
case 0x11C4:
return "Document Technologies Ind.";
case 0x11C5:
return "Shiva Corporatin";
case 0x11C6:
return "Dainippon Screen Mfg. Co";
case 0x11C7:
return "D.C.M. Data Systems";
case 0x11C8:
return "Dolphin Interconnect Solutions";
case 0x11C9:
return "MAGMA";
case 0x11CA:
return "LSI Systems Inc";
case 0x11CB:
return "Specialix International Ltd.";
case 0x11CC:
return "Michels & Kleberhoff Computer GmbH";
case 0x11CD:
return "HAL Computer Systems Inc.";
case 0x11CE:
return "Primary Rate Inc";
case 0x11CF:
return "Pioneer Electronic Corporation";
case 0x11D0:
return "BAE SYSTEMS - Manassas";
case 0x11D1:
return "AuraVision Corporation";
case 0x11D2:
return "Intercom Inc.";
case 0x11D3:
return "Trancell Systems Inc";
case 0x11D4:
return "Analog Devices, Inc.";
case 0x11D5:
return "Tahoma Technology";
case 0x11D6:
return "Tekelec Technologies";
case 0x11D7:
return "TRENTON Technology, Inc.";
case 0x11D8:
return "Image Technologies Development";
case 0x11D9:
return "Tec Corporation";
case 0x11DA:
return "Novell";
case 0x11DB:
return "Sega Enterprises Ltd";
case 0x11DC:
return "Questra Corp";
case 0x11DD:
return "Crosfield Electronics Ltd";
case 0x11DE:
return "Zoran Corporation";
case 0x11E1:
return "Gec Plessey Semi Inc";
case 0x11E2:
return "Samsung Information Systems America";
case 0x11E3:
return "Quicklogic Corp";
case 0x11E4:
return "Second Wave Inc";
case 0x11E5:
return "IIX Consulting";
case 0x11E6:
return "Mitsui-Zosen System Research";
case 0x11E8:
return "Digital Processing Systems Inc";
case 0x11E9:
return "Highwater Designs Ltd";
case 0x11EA:
return "Elsag Bailey";
case 0x11EB:
return "Formation, Inc";
case 0x11EC:
return "Coreco Inc";
case 0x11ED:
return "Mediamatics";
case 0x11EE:
return "Dome Imaging Systems Inc";
case 0x11EF:
return "Nicolet Technologies BV";
case 0x11F0:
return "Triya";
case 0x11F2:
return "Picture Tel Japan KK";
case 0x11F3:
return "Keithley Instruments, Inc";
case 0x11F4:
return "Kinetic Systems Corporation";
case 0x11F5:
return "Computing Devices Intl";
case 0x11F6:
return "Powermatic Data Systems Ltd";
case 0x11F7:
return "Scientific Atlanta";
case 0x11F8:
return "PMC-Sierra Inc.";
case 0x11F9:
return "I-Cube Inc";
case 0x11FA:
return "Kasan Electronics Co Ltd";
case 0x11FB:
return "Datel Inc";
case 0x11FD:
return "High Street Consultants";
case 0x11FE:
return "Comtrol Corp";
case 0x11FF:
return "Scion Corp";
case 0x1200:
return "CSS Corp";
case 0x1201:
return "Vista Controls Corp";
case 0x1202:
return "Network General Corp";
case 0x1203:
return "Bayer Corporation Agfa Div";
case 0x1204:
return "Lattice Semiconductor Corp";
case 0x1205:
return "Array Corp";
case 0x1206:
return "Amdahl Corp";
case 0x1208:
return "Parsytec GmbH";
case 0x1209:
return "Sci Systems Inc";
case 0x120A:
return "Synaptel";
case 0x120B:
return "Adaptive Solutions";
case 0x120D:
return "Compression Labs Inc.";
case 0x120E:
return "Cyclades Corporation";
case 0x120F:
return "Essential Communications";
case 0x1210:
return "Hyperparallel Technologies";
case 0x1211:
return "Braintech Inc";
case 0x1213:
return "Applied Intelligent Systems Inc";
case 0x1214:
return "Performance Technologies Inc";
case 0x1215:
return "Interware Co Ltd";
case 0x1216:
return "Purup-Eskofot A/S";
case 0x1217:
return "O2Micro Inc";
case 0x1218:
return "Hybricon Corp";
case 0x1219:
return "First Virtual Corp";
case 0x121A:
return "3dfx Interactive Inc";
case 0x121B:
return "Advanced Telecommunications Modules";
case 0x121C:
return "Nippon Texa Co Ltd";
case 0x121D:
return "LiPPERT Embedded Computers GmbH";
case 0x121E:
return "CSPI";
case 0x121F:
return "Arcus Technology Inc";
case 0x1220:
return "Ariel Corporation";
case 0x1221:
return "Contec Microelectronics Europe BV";
case 0x1222:
return "Ancor Communications Inc";
case 0x1223:
return "Artesyn Embedded Technologies";
case 0x1224:
return "Interactive Images";
case 0x1225:
return "Power I/O Inc.";
case 0x1227:
return "Tech-Source";
case 0x1228:
return "Norsk Elektro Optikk A/S";
case 0x1229:
return "Data Kinesis Inc.";
case 0x122A:
return "Integrated Telecom";
case 0x122B:
return "LG Industrial Systems Co. Ltd.";
case 0x122C:
return "sci-worx GmbH";
case 0x122D:
return "Aztech System Ltd";
case 0x122E:
return "Absolute Analysis";
case 0x122F:
return "Andrew Corp.";
case 0x1230:
return "Fishcamp Engineering";
case 0x1231:
return "Woodward McCoach Inc.";
case 0x1233:
return "Bus-Tech Inc.";
case 0x1234:
return "Technical Corp";
case 0x1236:
return "Sigma Designs, Inc";
case 0x1237:
return "Alta Technology Corp.";
case 0x1238:
return "Adtran";
case 0x1239:
return "The 3DO Company";
case 0x123A:
return "Visicom Laboratories Inc.";
case 0x123B:
return "Seeq Technology Inc.";
case 0x123C:
return "Century Systems Inc.";
case 0x123D:
return "Engineering Design Team Inc.";
case 0x123F:
return "C-Cube Microsystems";
case 0x1240:
return "Marathon Technologies Corp.";
case 0x1241:
return "DSC Communications";
case 0x1242:
return "JNI Corporation";
case 0x1243:
return "Delphax";
case 0x1244:
return "AVM AUDIOVISUELLES MKTG & Computer GmbH";
case 0x1245:
return "APD S.A.";
case 0x1246:
return "Dipix Technologies Inc";
case 0x1247:
return "Xylon Research Inc.";
case 0x1248:
return "Central Data Corp.";
case 0x1249:
return "Samsung Electronics Co. Ltd.";
case 0x124A:
return "AEG Electrocom GmbH";
case 0x124C:
return "Solitron Technologies Inc.";
case 0x124D:
return "Stallion Technologies";
case 0x124E:
return "Cylink";
case 0x124F:
return "Infortrend Technology Inc";
case 0x1250:
return "Hitachi Microcomputer System Ltd.";
case 0x1251:
return "VLSI Solution OY";
case 0x1253:
return "Guzik Technical Enterprises";
case 0x1254:
return "Linear Systems Ltd.";
case 0x1255:
return "Optibase Ltd.";
case 0x1256:
return "Perceptive Solutions Inc.";
case 0x1257:
return "Vertex Networks Inc.";
case 0x1258:
return "Gilbarco Inc.";
case 0x1259:
return "Allied Telesyn International";
case 0x125A:
return "ABB Power Systems";
case 0x125B:
return "Asix Electronics Corp.";
case 0x125C:
return "Aurora Technologies Inc.";
case 0x125D:
return "ESS Technology";
case 0x125E:
return "Specialvideo Engineering SRL";
case 0x125F:
return "Concurrent Technologies Inc.";
case 0x1260:
return "Intersil Corporation";
case 0x1261:
return "Matsushita-Kotobuki Electronics Indu";
case 0x1262:
return "ES Computer Co. Ltd.";
case 0x1263:
return "Sonic Solutions";
case 0x1264:
return "Aval Nagasaki Corp.";
case 0x1265:
return "Casio Computer Co. Ltd.";
case 0x1266:
return "Microdyne Corp.";
case 0x1267:
return "S.A. Telecommunications";
case 0x1268:
return "Tektronix";
case 0x1269:
return "Thomson-CSF/TTM";
case 0x126A:
return "Lexmark International Inc.";
case 0x126B:
return "Adax Inc.";
case 0x126C:
return "Nortel Networks Corp.";
case 0x126D:
return "Splash Technology Inc.";
case 0x126E:
return "Sumitomo Metal Industries Ltd.";
case 0x126F:
return "Silicon Motion";
case 0x1270:
return "Olympus Optical Co. Ltd.";
case 0x1271:
return "GW Instruments";
case 0x1272:
return "themrtaish";
case 0x1273:
return "Hughes Network Systems";
case 0x1274:
return "Ensoniq";
case 0x1275:
return "Network Appliance";
case 0x1276:
return "Switched Network Technologies Inc.";
case 0x1277:
return "Comstream";
case 0x1278:
return "Transtech Parallel Systems";
case 0x1279:
return "Transmeta Corp.";
case 0x127B:
return "Pixera Corp";
case 0x127C:
return "Crosspoint Solutions Inc.";
case 0x127D:
return "Vela Research LP";
case 0x127E:
return "Winnov L.P.";
case 0x127F:
return "Fujifilm";
case 0x1280:
return "Photoscript Group Ltd.";
case 0x1281:
return "Yokogawa Electronic Corp.";
case 0x1282:
return "Davicom Semiconductor Inc.";
case 0x1283:
return "Waldo";
case 0x1285:
return "Platform Technologies Inc.";
case 0x1286:
return "MAZeT GmbH";
case 0x1287:
return "LuxSonor Inc.";
case 0x1288:
return "Timestep Corp.";
case 0x1289:
return "AVC Technology Inc.";
case 0x128A:
return "Asante Technologies Inc.";
case 0x128B:
return "Transwitch Corp.";
case 0x128C:
return "Retix Corp.";
case 0x128D:
return "G2 Networks Inc.";
case 0x128F:
return "Tateno Dennou Inc.";
case 0x1290:
return "Sord Computer Corp.";
case 0x1291:
return "NCS Computer Italia";
case 0x1292:
return "Tritech Microelectronics Intl PTE";
case 0x1293:
return "Media Reality Technology";
case 0x1294:
return "Rhetorex Inc.";
case 0x1295:
return "Imagenation Corp.";
case 0x1296:
return "Kofax Image Products";
case 0x1297:
return "Shuttle Computer";
case 0x1298:
return "Spellcaster Telecommunications Inc.";
case 0x1299:
return "Knowledge Technology Laboratories";
case 0x129A:
return "Curtiss Wright Controls Electronic Systems";
case 0x129B:
return "Image Access";
case 0x129D:
return "CompCore Multimedia Inc.";
case 0x129E:
return "Victor Co. of Japan Ltd.";
case 0x129F:
return "OEC Medical Systems Inc.";
case 0x12A0:
return "Allen Bradley Co.";
case 0x12A1:
return "Simpact Inc";
case 0x12A2:
return "NewGen Systems Corp.";
case 0x12A3:
return "Lucent Technologies AMR";
case 0x12A4:
return "NTT Electronics Corp.";
case 0x12A5:
return "Vision Dynamics Ltd.";
case 0x12A6:
return "Scalable Networks Inc.";
case 0x12A7:
return "AMO GmbH";
case 0x12A8:
return "News Datacom";
case 0x12A9:
return "Xiotech Corp.";
case 0x12AA:
return "SDL Communications Inc.";
case 0x12AB:
return "Yuan Yuan Enterprise Co. Ltd.";
case 0x12AC:
return "MeasureX Corp.";
case 0x12AD:
return "MULTIDATA GmbH";
case 0x12AE:
return "Alteon Networks Inc.";
case 0x12AF:
return "TDK USA Corp.";
case 0x12B0:
return "Jorge Scientific Corp.";
case 0x12B1:
return "GammaLink";
case 0x12B2:
return "General Signal Networks";
case 0x12B3:
return "Interface Corp. Ltd.";
case 0x12B4:
return "Future Tel Inc.";
case 0x12B5:
return "Granite Systems Inc.";
case 0x12B7:
return "Acumen";
case 0x12B8:
return "Korg";
case 0x12B9:
return "3Com Corporation";
case 0x12BA:
return "Bittware, Inc";
case 0x12BB:
return "Nippon Unisoft Corp.";
case 0x12BC:
return "Array Microsystems";
case 0x12BD:
return "Computerm Corp.";
case 0x12BF:
return "Fujifilm Microdevices";
case 0x12C0:
return "Infimed";
case 0x12C1:
return "GMM Research Corp.";
case 0x12C2:
return "Mentec Ltd.";
case 0x12C3:
return "Holtek Microelectronics Inc.";
case 0x12C4:
return "Connect Tech Inc.";
case 0x12C5:
return "Picture Elements Inc.";
case 0x12C6:
return "Mitani Corp.";
case 0x12C7:
return "Dialogic Corp.";
case 0x12C8:
return "G Force Co. Ltd.";
case 0x12C9:
return "Gigi Operations";
case 0x12CA:
return "Integrated Computing Engines, Inc.";
case 0x12CB:
return "Antex Electronics Corp.";
case 0x12CC:
return "Pluto Technologies International";
case 0x12CD:
return "Aims Lab";
case 0x12CE:
return "Netspeed Inc.";
case 0x12CF:
return "Prophet Systems Inc.";
case 0x12D0:
return "GDE Systems Inc.";
case 0x12D1:
return "Huawei Technologies Co., Ltd.";
case 0x12D3:
return "Vingmed Sound A/S";
case 0x12D4:
return "Ulticom, Inc.";
case 0x12D5:
return "Equator Technologies";
case 0x12D6:
return "Analogic Corp.";
case 0x12D7:
return "Biotronic SRL";
case 0x12D8:
return "Pericom Semiconductor";
case 0x12D9:
return "Aculab Plc.";
case 0x12DA:
return "TrueTime";
case 0x12DB:
return "Annapolis Micro Systems Inc.";
case 0x12DC:
return "Symicron Computer Communication Ltd.";
case 0x12DD:
return "Management Graphics Inc.";
case 0x12DE:
return "Rainbow Technologies";
case 0x12DF:
return "SBS Technologies Inc.";
case 0x12E0:
return "Chase Research PLC";
case 0x12E1:
return "Nintendo Co. Ltd.";
case 0x12E2:
return "Datum Inc. Bancomm-Timing Division";
case 0x12E3:
return "Imation Corp. - Medical Imaging Syst";
case 0x12E4:
return "Brooktrout Technology Inc.";
case 0x12E6:
return "Cirel Systems";
case 0x12E7:
return "Sebring Systems Inc";
case 0x12E8:
return "CRISC Corp.";
case 0x12E9:
return "GE Spacenet";
case 0x12EB:
return "Aureal Semiconductor";
case 0x12EC:
return "3A International Inc.";
case 0x12ED:
return "Optivision Inc.";
case 0x12EE:
return "Orange Micro, Inc.";
case 0x12EF:
return "Vienna Systems";
case 0x12F0:
return "Pentek";
case 0x12F1:
return "Sorenson Vision Inc.";
case 0x12F2:
return "Gammagraphx Inc.";
case 0x12F4:
return "Megatel";
case 0x12F5:
return "Forks";
case 0x12F7:
return "Cognex";
case 0x12F8:
return "Electronic-Design GmbH";
case 0x12F9:
return "FourFold Technologies";
case 0x12FB:
return "Spectrum Signal Processing";
case 0x12FC:
return "Capital Equipment Corp";
case 0x12FE:
return "esd Electronic System Design GmbH";
case 0x1303:
return "Innovative Integration";
case 0x1304:
return "Juniper Networks Inc.";
case 0x1307:
return "ComputerBoards";
case 0x1308:
return "Jato Technologies Inc.";
case 0x130A:
return "Mitsubishi Electric Microcomputer";
case 0x130B:
return "Colorgraphic Communications Corp";
case 0x130F:
return "Advanet Inc.";
case 0x1310:
return "Gespac";
case 0x1312:
return "Microscan Systems Inc";
case 0x1313:
return "Yaskawa Electric Co.";
case 0x1316:
return "Teradyne Inc.";
case 0x1317:
return "ADMtek Inc";
case 0x1318:
return "Packet Engines, Inc.";
case 0x1319:
return "Forte Media";
case 0x131F:
return "SIIG";
case 0x1325:
return "austriamicrosystems";
case 0x1326:
return "Seachange International";
case 0x1328:
return "CIFELLI SYSTEMS CORPORATION";
case 0x1331:
return "RadiSys Corporation";
case 0x1332:
return "Curtiss-Wright Controls Embedded Computing";
case 0x1335:
return "Videomail Inc.";
case 0x133D:
return "Prisa Networks";
case 0x133F:
return "SCM Microsystems";
case 0x1342:
return "Promax Systems Inc";
case 0x1344:
return "Micron Technology, Inc.";
case 0x1347:
return "Spectracom Corporation";
case 0x134A:
return "DTC Technology Corp.";
case 0x134B:
return "ARK Research Corp.";
case 0x134C:
return "Chori Joho System Co. Ltd";
case 0x134D:
return "PCTEL Inc.";
case 0x135A:
return "Brain Boxes Limited";
case 0x135B:
return "Giganet Inc.";
case 0x135C:
return "Quatech Inc";
case 0x135D:
return "ABB Network Partner AB";
case 0x135E:
return "Sealevel Systems Inc.";
case 0x135F:
return "I-Data International A-S";
case 0x1360:
return "Meinberg Funkuhren GmbH & Co. KG";
case 0x1361:
return "Soliton Systems K.K.";
case 0x1363:
return "Phoenix Technologies Ltd";
case 0x1365:
return "Hypercope Corp.";
case 0x1366:
return "Teijin Seiki Co. Ltd.";
case 0x1367:
return "Hitachi Zosen Corporation";
case 0x1368:
return "Skyware Corporation";
case 0x1369:
return "Digigram";
case 0x136B:
return "Kawasaki Steel Corporation";
case 0x136C:
return "Adtek System Science Co Ltd";
case 0x1375:
return "Boeing - Sunnyvale";
case 0x137A:
return "Mark Of The Unicorn Inc";
case 0x137B:
return "PPT Vision";
case 0x137C:
return "Iwatsu Electric Co Ltd";
case 0x137D:
return "Dynachip Corporation";
case 0x137E:
return "Patriot Scientific Corp.";
case 0x1380:
return "Sanritz Automation Co LTC";
case 0x1381:
return "Brains Co. Ltd";
case 0x1382:
return "Marian - Electronic & Software";
case 0x1384:
return "Stellar Semiconductor Inc";
case 0x1385:
return "Netgear";
case 0x1387:
return "Curtiss-Wright Controls Electronic Systems";
case 0x1388:
return "Hitachi Information Technology Co Ltd";
case 0x1389:
return "Applicom International";
case 0x138A:
return "Validity Sensors, Inc.";
case 0x138B:
return "Tokimec Inc";
case 0x138E:
return "Basler GMBH";
case 0x138F:
return "Patapsco Designs Inc";
case 0x1390:
return "Concept Development Inc.";
case 0x1393:
return "Moxa Technologies Co Ltd";
case 0x1394:
return "Level One Communications";
case 0x1395:
return "Ambicom Inc";
case 0x1396:
return "Cipher Systems Inc";
case 0x1397:
return "Cologne Chip Designs GmbH";
case 0x1398:
return "Clarion Co. Ltd";
case 0x139A:
return "Alacritech Inc";
case 0x139D:
return "Xstreams PLC/ EPL Limited";
case 0x139E:
return "Echostar Data Networks";
case 0x13A0:
return "Crystal Group Inc";
case 0x13A1:
return "Kawasaki Heavy Industries Ltd";
case 0x13A3:
return "HI-FN Inc.";
case 0x13A4:
return "Rascom Inc";
case 0x13A7:
return "amc330";
case 0x13A8:
return "Exar Corp.";
case 0x13A9:
return "Siemens Healthcare";
case 0x13AA:
return "Nortel Networks - BWA Division";
case 0x13AF:
return "T.Sqware";
case 0x13B1:
return "Tamura Corporation";
case 0x13B4:
return "Wellbean Co Inc";
case 0x13B5:
return "ARM Ltd";
case 0x13B6:
return "DLoG Gesellschaft für elektronische Datentechnik mbH";
case 0x13B8:
return "Nokia Telecommunications OY";
case 0x13BD:
return "Sharp Corporation";
case 0x13BF:
return "Sharewave Inc";
case 0x13C0:
return "Microgate Corp.";
case 0x13C1:
return "LSI";
case 0x13C2:
return "Technotrend Systemtechnik GMBH";
case 0x13C3:
return "Janz Computer AG";
case 0x13C7:
return "Blue Chip Technology Ltd";
case 0x13CC:
return "Metheus Corporation";
case 0x13CF:
return "Studio Audio & Video Ltd";
case 0x13D0:
return "B2C2 Inc";
case 0x13D1:
return "AboCom Systems, Inc";
case 0x13D4:
return "Graphics Microsystems Inc";
case 0x13D6:
return "K.I. Technology Co Ltd";
case 0x13D7:
return "Toshiba Engineering Corporation";
case 0x13D8:
return "Phobos Corporation";
case 0x13D9:
return "Apex Inc";
case 0x13DC:
return "Netboost Corporation";
case 0x13DE:
return "ABB Robotics Products AB";
case 0x13DF:
return "E-Tech Inc.";
case 0x13E0:
return "GVC Corporation";
case 0x13E3:
return "Nest Inc";
case 0x13E4:
return "Calculex Inc";
case 0x13E5:
return "Telesoft Design Ltd";
case 0x13E9:
return "Intraserver Technology Inc";
case 0x13EA:
return "Dallas Semiconductor";
case 0x13F0:
return "IC Plus Corporation";
case 0x13F1:
return "OCE - Industries S.A.";
case 0x13F4:
return "Troika Networks Inc";
case 0x13F6:
return "C-Media Electronics Inc.";
case 0x13F9:
return "NTT Advanced Technology Corp.";
case 0x13FA:
return "Pentland Systems Ltd.";
case 0x13FB:
return "Aydin Corp";
case 0x13FD:
return "Micro Science Inc";
case 0x13FE:
return "Advantech Co., Ltd.";
case 0x13FF:
return "Silicon Spice Inc.";
case 0x1400:
return "ArtX Inc";
case 0x1402:
return "Meilhaus Electronic GmbH Germany";
case 0x1404:
return "Fundamental Software Inc";
case 0x1406:
return "Oce Print Logics Technologies S.A.";
case 0x1407:
return "Lava Computer MFG Inc.";
case 0x1408:
return "Aloka Co. Ltd";
case 0x1409:
return "SUNIX Co., Ltd.";
case 0x140A:
return "DSP Research Inc";
case 0x140B:
return "Ramix Inc";
case 0x140D:
return "Matsushita Electric Works Ltd";
case 0x140F:
return "Salient Systems Corp";
case 0x1412:
return "IC Ensemble, Inc.";
case 0x1413:
return "Addonics";
case 0x1415:
return "Oxford Semiconductor Ltd - now part of PLX Technology ";
case 0x1418:
return "Kyushu Electronics Systems Inc";
case 0x1419:
return "Excel Switching Corp";
case 0x141B:
return "Zoom Telephonics Inc";
case 0x141E:
return "Fanuc Co. Ltd";
case 0x141F:
return "Visiontech Ltd";
case 0x1420:
return "Psion Dacom PLC";
case 0x1425:
return "Chelsio Communications";
case 0x1428:
return "Edec Co Ltd";
case 0x1429:
return "Unex Technology Corp.";
case 0x142A:
return "Kingmax Technology Inc";
case 0x142B:
return "Radiolan";
case 0x142C:
return "Minton Optic Industry Co Ltd";
case 0x142D:
return "Pixstream Inc";
case 0x1430:
return "ITT Aerospace/Communications Division";
case 0x1433:
return "Eltec Elektronik AG";
case 0x1435:
return "RTD Embedded Technologies, Inc.";
case 0x1436:
return "CIS Technology Inc";
case 0x1437:
return "Nissin Inc Co";
case 0x1438:
return "Atmel-Dream";
case 0x143F:
return "Lightwell Co Ltd - Zax Division";
case 0x1441:
return "Agie SA.";
case 0x1443:
return "Unibrain S.A.";
case 0x1445:
return "Logical Co Ltd";
case 0x1446:
return "Graphin Co., LTD";
case 0x1447:
return "Aim GMBH";
case 0x1448:
return "Alesis Studio";
case 0x144A:
return "ADLINK Technology Inc";
case 0x144B:
return "Loronix Information Systems, Inc.";
case 0x144D:
return "sanyo";
case 0x1450:
return "Octave Communications Ind.";
case 0x1451:
return "SP3D Chip Design GMBH";
case 0x1453:
return "Mycom Inc";
case 0x1458:
return "Giga-Byte Technologies";
case 0x145C:
return "Cryptek";
case 0x145F:
return "Baldor Electric Company";
case 0x1460:
return "Dynarc Inc";
case 0x1462:
return "Micro-Star International Co Ltd";
case 0x1463:
return "Fast Corporation";
case 0x1464:
return "Interactive Circuits & Systems Ltd";
case 0x1468:
return "Ambit Microsystems Corp.";
case 0x1469:
return "Cleveland Motion Controls";
case 0x146C:
return "Ruby Tech Corp.";
case 0x146D:
return "Tachyon Inc.";
case 0x146E:
return "WMS Gaming";
case 0x1471:
return "Integrated Telecom Express Inc";
case 0x1473:
return "Zapex Technologies Inc";
case 0x1474:
return "Doug Carson & Associates";
case 0x1477:
return "Net Insight";
case 0x1478:
return "Diatrend Corporation";
case 0x147B:
return "Abit Computer Corp.";
case 0x147F:
return "Nihon Unisys Ltd.";
case 0x1482:
return "Isytec - Integrierte Systemtechnik Gmbh";
case 0x1483:
return "Labway Coporation";
case 0x1485:
return "Erma - Electronic GMBH";
case 0x1489:
return "KYE Systems Corporation";
case 0x148A:
return "Opto 22";
case 0x148B:
return "Innomedialogic Inc.";
case 0x148C:
return "C.P. Technology Co. Ltd";
case 0x148D:
return "Digicom Systems Inc.";
case 0x148E:
return "OSI Plus Corporation";
case 0x148F:
return "Plant Equipment Inc.";
case 0x1490:
return "TC Labs Pty Ltd.";
case 0x1491:
return "Futronic ";
case 0x1493:
return "Maker Communications";
case 0x1495:
return "Tokai Communications Industry Co. Ltd";
case 0x1496:
return "Joytech Computer Co. Ltd.";
case 0x1497:
return "SMA Technologie AG";
case 0x1498:
return "Tews Technologies";
case 0x1499:
return "Micro-Technology Co Ltd";
case 0x149A:
return "Andor Technology Ltd";
case 0x149B:
return "Seiko Instruments Inc";
case 0x149E:
return "Mapletree Networks Inc.";
case 0x149F:
return "Lectron Co Ltd";
case 0x14A0:
return "Softing AG";
case 0x14A2:
return "Millennium Engineering Inc";
case 0x14A4:
return "GVC/BCM Advanced Research";
case 0x14A9:
return "Hivertec Inc.";
case 0x14AB:
return "Mentor Graphics Corp.";
case 0x14B1:
return "Nextcom K.K.";
case 0x14B3:
return "Xpeed Inc.";
case 0x14B4:
return "Philips Business Electronics B.V.";
case 0x14B5:
return "Creamware GmbH";
case 0x14B6:
return "Quantum Data Corp.";
case 0x14B7:
return "Proxim Inc.";
case 0x14B9:
return "Aironet Wireless Communication";
case 0x14BA:
return "Internix Inc.";
case 0x14BB:
return "Semtech Corporation";
case 0x14BE:
return "L3 Communications";
case 0x14C0:
return "Compal Electronics, Inc.";
case 0x14C1:
return "Myricom Inc.";
case 0x14C2:
return "DTK Computer";
case 0x14C4:
return "Iwasaki Information Systems Co Ltd";
case 0x14C5:
return "ABB AB (Sweden)";
case 0x14C6:
return "Data Race Inc";
case 0x14C7:
return "Modular Technology Ltd.";
case 0x14C8:
return "Turbocomm Tech Inc";
case 0x14C9:
return "Odin Telesystems Inc";
case 0x14CB:
return "Billionton Systems Inc./Cadmus Micro Inc";
case 0x14CD:
return "Universal Scientific Ind.";
case 0x14CF:
return "TEK Microsystems Inc.";
case 0x14D4:
return "Panacom Technology Corporation";
case 0x14D5:
return "Nitsuko Corporation";
case 0x14D6:
return "Accusys Inc";
case 0x14D7:
return "Hirakawa Hewtech Corp";
case 0x14D8:
return "Hopf Elektronik GMBH";
case 0x14D9:
return "Alpha Processor Inc";
case 0x14DB:
return "Avlab Technology Inc.";
case 0x14DC:
return "Amplicon Liveline Limited";
case 0x14DD:
return "Imodl Inc.";
case 0x14DE:
return "Applied Integration Corporation";
case 0x14E3:
return "Amtelco";
case 0x14E4:
return "Broadcom";
case 0x14EA:
return "Planex Communications, Inc.";
case 0x14EB:
return "Seiko Epson Corporation";
case 0x14EC:
return "Acqiris";
case 0x14ED:
return "Datakinetics Ltd";
case 0x14EF:
return "Carry Computer Eng. Co Ltd";
case 0x14F1:
return "Conexant";
case 0x14F2:
return "Mobility Electronics, Inc.";
case 0x14F4:
return "Tokyo Electronic Industry Co. Ltd.";
case 0x14F5:
return "Sopac Ltd";
case 0x14F6:
return "Coyote Technologies LLC";
case 0x14F7:
return "Wolf Technology Inc";
case 0x14F8:
return "Audiocodes Inc";
case 0x14F9:
return "AG Communications";
case 0x14FB:
return "Transas Marine (UK) Ltd";
case 0x14FC:
return "Quadrics Ltd";
case 0x14FD:
return "Silex Technology Inc.";
case 0x14FE:
return "Archtek Telecom Corp.";
case 0x14FF:
return "Twinhead International Corp.";
case 0x1501:
return "Banksoft Canada Ltd";
case 0x1502:
return "Mitsubishi Electric Logistics Support Co";
case 0x1503:
return "Kawasaki LSI USA Inc";
case 0x1504:
return "Kaiser Electronics";
case 0x1506:
return "Chameleon Systems Inc";
case 0x1507:
return "Htec Ltd.";
case 0x1509:
return "First International Computer Inc";
case 0x150B:
return "Yamashita Systems Corp";
case 0x150C:
return "Kyopal Co Ltd";
case 0x150D:
return "Warpspped Inc";
case 0x150E:
return "C-Port Corporation";
case 0x150F:
return "Intec GMBH";
case 0x1510:
return "Behavior Tech Computer Corp";
case 0x1511:
return "Centillium Technology Corp";
case 0x1512:
return "Rosun Technologies Inc";
case 0x1513:
return "Raychem";
case 0x1514:
return "TFL LAN Inc";
case 0x1515:
return "ICS Advent";
case 0x1516:
return "Myson Technology Inc";
case 0x1517:
return "Echotek Corporation";
case 0x1518:
return "Kontron Modular Computers GmbH (PEP Modular Computers GMBH)";
case 0x1519:
return "Telefon Aktiebolaget LM Ericsson";
case 0x151A:
return "Globetek Inc.";
case 0x151B:
return "Combox Ltd";
case 0x151C:
return "Digital Audio Labs Inc";
case 0x151D:
return "Fujitsu Computer Products Of America";
case 0x151E:
return "Matrix Corp.";
case 0x151F:
return "Topic Semiconductor Corp";
case 0x1520:
return "Chaplet System Inc";
case 0x1521:
return "Bell Corporation";
case 0x1522:
return "Mainpine Limited";
case 0x1523:
return "Music Semiconductors";
case 0x1524:
return "ENE Technology Inc";
case 0x1525:
return "Impact Technologies";
case 0x1526:
return "ISS Inc";
case 0x1527:
return "Solectron";
case 0x1528:
return "Acksys";
case 0x1529:
return "American Microsystems Inc";
case 0x152A:
return "Quickturn Design Systems";
case 0x152B:
return "Flytech Technology Co Ltd";
case 0x152C:
return "Macraigor Systems LLC";
case 0x152D:
return "Quanta Computer Inc";
case 0x152E:
return "Melec Inc";
case 0x152F:
return "Philips - Crypto";
case 0x1532:
return "Echelon Corporation";
case 0x1533:
return "Baltimore";
case 0x1534:
return "Road Corporation";
case 0x1535:
return "Evergreen Technologies Inc";
case 0x1537:
return "Datalex Communcations";
case 0x1538:
return "Aralion Inc.";
case 0x1539:
return "Atelier Informatiques et Electronique Et";
case 0x153A:
return "ONO Sokki";
case 0x153B:
return "Terratec Electronic GMBH";
case 0x153C:
return "Antal Electronic";
case 0x153D:
return "Filanet Corporation";
case 0x153E:
return "Techwell Inc";
case 0x153F:
return "MIPS Technologies, Inc";
case 0x1540:
return "Provideo Multimedia Co Ltd";
case 0x1541:
return "Telocity Inc.";
case 0x1542:
return "Vivid Technology Inc";
case 0x1543:
return "Silicon Laboratories";
case 0x1544:
return "DCM Technologies Ltd.";
case 0x1545:
return "VisionTek";
case 0x1546:
return "IOI Technology Corp.";
case 0x1547:
return "Mitutoyo Corporation";
case 0x1548:
return "Jet Propulsion Laboratory";
case 0x1549:
return "Interconnect Systems Solutions";
case 0x154A:
return "Max Technologies Inc.";
case 0x154B:
return "Computex Co Ltd";
case 0x154C:
return "Visual Technology Inc.";
case 0x154D:
return "PAN International Industrial Corp";
case 0x154E:
return "Servotest Ltd";
case 0x154F:
return "Stratabeam Technology";
case 0x1550:
return "Open Network Co Ltd";
case 0x1551:
return "Smart Electronic Development GMBH";
case 0x1553:
return "Chicony Electronics Co Ltd";
case 0x1554:
return "Prolink Microsystems Corp.";
case 0x1555:
return "Gesytec GmbH";
case 0x1556:
return "PLDA";
case 0x1557:
return "Mediastar Co. Ltd";
case 0x1558:
return "Clevo/Kapok Computer";
case 0x1559:
return "SI Logic Ltd";
case 0x155A:
return "Innomedia Inc";
case 0x155B:
return "Protac International Corp";
case 0x155C:
return "s";
case 0x155D:
return "MAC System Co Ltd";
case 0x155E:
return "KUKA Roboter GmbH";
case 0x155F:
return "Perle Systems Limited";
case 0x1560:
return "Terayon Communications Systems";
case 0x1561:
return "Viewgraphics Inc";
case 0x1562:
return "Symbol Technologies, Inc.";
case 0x1563:
return "A-Trend Technology Co Ltd";
case 0x1564:
return "Yamakatsu Electronics Industry Co Ltd";
case 0x1565:
return "Biostar Microtech Intl Corp";
case 0x1566:
return "Ardent Technologies Inc";
case 0x1567:
return "Jungsoft";
case 0x1568:
return "DDK Electronics Inc";
case 0x1569:
return "Palit Microsystems Inc";
case 0x156A:
return "Avtec Systems Inc";
case 0x156B:
return "S2io Inc";
case 0x156C:
return "Vidac Electronics GMBH";
case 0x156D:
return "Alpha-Top Corp";
case 0x156E:
return "Alfa Inc.";
case 0x156F:
return "M-Systems Flash Disk Pioneers Ltd";
case 0x1570:
return "Lecroy Corporation";
case 0x1571:
return "Contemporary Controls";
case 0x1572:
return "Otis Elevator Company";
case 0x1573:
return "Lattice - Vantis";
case 0x1574:
return "Fairchild Semiconductor";
case 0x1575:
return "Voltaire Advanced Data Security Ltd";
case 0x1576:
return "Viewcast Com";
case 0x1578:
return "Hitt";
case 0x1579:
return "Dual Technology Corporation";
case 0x157A:
return "Japan Elecronics Ind. Inc";
case 0x157B:
return "Star Multimedia Corp.";
case 0x157C:
return "Eurosoft (UK)";
case 0x157D:
return "Gemflex Networks";
case 0x157E:
return "Transition Networks";
case 0x157F:
return "PX Instruments Technology Ltd";
case 0x1580:
return "Primex Aerospace Co.";
case 0x1581:
return "SEH Computertechnik GMBH";
case 0x1582:
return "Cytec Corporation";
case 0x1583:
return "Inet Technologies Inc";
case 0x1584:
return "Vetronix Corporation Engenharia Ltda";
case 0x1585:
return "Marconi Commerce Systems SRL";
case 0x1586:
return "Lancast Inc";
case 0x1587:
return "Konica Corporation";
case 0x1588:
return "Solidum Systems Corp";
case 0x1589:
return "Atlantek Microsystems Pty Ltd";
case 0x158A:
return "Digalog Systems Inc";
case 0x158B:
return "Allied Data Technologies";
case 0x158C:
return "Hitachi Semiconductor & Devices Sales Co";
case 0x158D:
return "Point Multimedia Systems";
case 0x158E:
return "Lara Technology Inc";
case 0x158F:
return "Ditect Coop";
case 0x1590:
return "3pardata Inc.";
case 0x1591:
return "ARN";
case 0x1592:
return "Syba Tech Ltd.";
case 0x1593:
return "Bops Inc";
case 0x1594:
return "Netgame Ltd";
case 0x1595:
return "Diva Systems Corp.";
case 0x1596:
return "Folsom Research Inc";
case 0x1597:
return "Memec Design Services";
case 0x1598:
return "Granite Microsystems";
case 0x1599:
return "Delta Electronics Inc";
case 0x159A:
return "General Instrument";
case 0x159B:
return "Faraday Technology Corp";
case 0x159C:
return "Stratus Computer Systems";
case 0x159D:
return "Ningbo Harrison Electronics Co Ltd";
case 0x159E:
return "A-Max Technology Co Ltd";
case 0x159F:
return "Galea Network Security";
case 0x15A0:
return "Compumaster SRL";
case 0x15A1:
return "Geocast Network Systems Inc";
case 0x15A2:
return "Catalyst Enterprises Inc";
case 0x15A3:
return "Italtel";
case 0x15A4:
return "X-Net OY";
case 0x15A5:
return "Toyota MACS Inc";
case 0x15A6:
return "Sunlight Ultrasound Technologies Ltd";
case 0x15A7:
return "SSE Telecom Inc";
case 0x15A8:
return "Shanghai Communications Technologies Cen";
case 0x15AA:
return "Moreton Bay";
case 0x15AB:
return "Bluesteel Networks Inc";
case 0x15AC:
return "North Atlantic Instruments";
case 0x15AD:
return "VMware Inc.";
case 0x15AE:
return "Amersham Pharmacia Biotech";
case 0x15B0:
return "Zoltrix International Limited";
case 0x15B1:
return "Source Technology Inc";
case 0x15B2:
return "Mosaid Technologies Inc.";
case 0x15B3:
return "Mellanox Technology";
case 0x15B4:
return "CCI/Triad";
case 0x15B5:
return "Cimetrics Inc";
case 0x15B6:
return "Texas Memory Systems Inc";
case 0x15B7:
return "Sandisk Corp.";
case 0x15B8:
return "Addi-Data GMBH";
case 0x15B9:
return "Maestro Digital Communications";
case 0x15BA:
return "Impacct Technology Corp";
case 0x15BB:
return "Portwell Inc";
case 0x15BC:
return "Agilent Technologies";
case 0x15BD:
return "DFI Inc.";
case 0x15BE:
return "Sola Electronics";
case 0x15BF:
return "High Tech Computer Corp (HTC)";
case 0x15C0:
return "BVM Limited";
case 0x15C1:
return "Quantel";
case 0x15C2:
return "Newer Technology Inc";
case 0x15C3:
return "Taiwan Mycomp Co Ltd";
case 0x15C4:
return "EVSX Inc";
case 0x15C5:
return "Procomp Informatics Ltd";
case 0x15C6:
return "Technical University Of Budapest";
case 0x15C7:
return "Tateyama System Laboratory Co Ltd";
case 0x15C8:
return "Penta Media Co. Ltd";
case 0x15C9:
return "Serome Technology Inc";
case 0x15CA:
return "Bitboys OY";
case 0x15CB:
return "AG Electronics Ltd";
case 0x15CC:
return "Hotrail Inc.";
case 0x15CD:
return "Dreamtech Co Ltd";
case 0x15CE:
return "Genrad Inc.";
case 0x15CF:
return "Hilscher GMBH";
case 0x15D1:
return "Infineon Technologies AG";
case 0x15D2:
return "FIC (First International Computer Inc)";
case 0x15D3:
return "NDS Technologies Israel Ltd";
case 0x15D4:
return "Iwill Corporation";
case 0x15D5:
return "Tatung Co.";
case 0x15D6:
return "Entridia Corporation";
case 0x15D7:
return "Rockwell-Collins Inc";
case 0x15D8:
return "Cybernetics Technology Co Ltd";
case 0x15D9:
return "Super Micro Computer Inc";
case 0x15DA:
return "Cyberfirm Inc.";
case 0x15DB:
return "Applied Computing Systems Inc.";
case 0x15DC:
return "Litronic Inc.";
case 0x15DD:
return "Sigmatel Inc.";
case 0x15DE:
return "Malleable Technologies Inc";
case 0x15E0:
return "Cacheflow Inc";
case 0x15E1:
return "Voice Technologies Group";
case 0x15E2:
return "Quicknet Technologies Inc";
case 0x15E3:
return "Networth Technologies Inc";
case 0x15E4:
return "VSN Systemen BV";
case 0x15E5:
return "Valley Technologies Inc";
case 0x15E6:
return "Agere Inc.";
case 0x15E7:
return "GET Engineering Corp.";
case 0x15E8:
return "National Datacomm Corp.";
case 0x15E9:
return "Pacific Digital Corp.";
case 0x15EA:
return "Tokyo Denshi Sekei K.K.";
case 0x15EB:
return "Drsearch GMBH";
case 0x15EC:
return "Beckhoff Automation GmbH";
case 0x15ED:
return "Macrolink Inc";
case 0x15EE:
return "IN Win Development Inc.";
case 0x15EF:
return "Intelligent Paradigm Inc";
case 0x15F0:
return "B-Tree Systems Inc";
case 0x15F1:
return "Times N Systems Inc";
case 0x15F2:
return "SPOT Imaging Solutions a division of Diagnostic Instruments, Inc";
case 0x15F3:
return "Digitmedia Corp.";
case 0x15F4:
return "Valuesoft";
case 0x15F5:
return "Power Micro Research";
case 0x15F6:
return "Extreme Packet Device Inc";
case 0x15F7:
return "Banctec";
case 0x15F8:
return "Koga Electronics Co";
case 0x15F9:
return "Zenith Electronics Co";
case 0x15FA:
return "Axzam Corporation";
case 0x15FB:
return "Zilog Inc.";
case 0x15FC:
return "Techsan Electronics Co Ltd";
case 0x15FD:
return "N-Cubed.Net";
case 0x15FE:
return "Kinpo Electronics Inc";
case 0x15FF:
return "Fastpoint Technologies Inc.";
case 0x1600:
return "Northrop Grumman - Canada Ltd";
case 0x1601:
return "Tenta Technology";
case 0x1602:
return "Prosys-TEC Inc.";
case 0x1603:
return "Nokia Wireless Business Communications";
case 0x1604:
return "Central System Research Co Ltd";
case 0x1605:
return "Pairgain Technologies";
case 0x1606:
return "Europop AG";
case 0x1607:
return "Lava Semiconductor Manufacturing Inc.";
case 0x1608:
return "Automated Wagering International";
case 0x1609:
return "Sciemetric Instruments Inc";
case 0x160A:
return "Kollmorgen Servotronix";
case 0x160B:
return "Onkyo Corp.";
case 0x160C:
return "Oregon Micro Systems Inc.";
case 0x160D:
return "Aaeon Electronics Inc";
case 0x160E:
return "CML Emergency Services";
case 0x160F:
return "ITEC Co Ltd";
case 0x1610:
return "Tottori Sanyo Electric Co Ltd";
case 0x1611:
return "Bel Fuse Inc.";
case 0x1612:
return "Telesynergy Research Inc.";
case 0x1613:
return "System Craft Inc.";
case 0x1614:
return "Jace Tech Inc.";
case 0x1615:
return "Equus Computer Systems Inc";
case 0x1616:
return "Iotech Inc.";
case 0x1617:
return "Rapidstream Inc";
case 0x1618:
return "Esec SA";
case 0x1619:
return "FarSite Communications Limited";
case 0x161B:
return "Mobilian Israel Ltd";
case 0x161C:
return "Berkshire Products";
case 0x161D:
return "Gatec";
case 0x161E:
return "Kyoei Sangyo Co Ltd";
case 0x161F:
return "Arima Computer Corporation";
case 0x1620:
return "Sigmacom Co Ltd";
case 0x1621:
return "Lynx Studio Technology Inc";
case 0x1622:
return "Nokia Home Communications";
case 0x1623:
return "KRF Tech Ltd";
case 0x1624:
return "CE Infosys GMBH";
case 0x1625:
return "Warp Nine Engineering";
case 0x1626:
return "TDK Semiconductor Corp.";
case 0x1627:
return "BCom Electronics Inc";
case 0x1629:
return "Kongsberg Spacetec a.s.";
case 0x162A:
return "Sejin Computerland Co Ltd";
case 0x162B:
return "Shanghai Bell Company Limited";
case 0x162C:
return "C&H Technologies Inc";
case 0x162D:
return "Reprosoft Co Ltd";
case 0x162E:
return "Margi Systems Inc";
case 0x162F:
return "Rohde & Schwarz GMBH & Co KG";
case 0x1630:
return "Sky Computers Inc";
case 0x1631:
return "NEC Computer International";
case 0x1632:
return "Verisys Inc";
case 0x1633:
return "Adac Corporation";
case 0x1634:
return "Visionglobal Network Corp.";
case 0x1635:
return "Decros / S.ICZ a.s.";
case 0x1636:
return "Jean Company Ltd";
case 0x1637:
return "NSI";
case 0x1638:
return "Eumitcom Technology Inc";
case 0x163A:
return "Air Prime Inc";
case 0x163B:
return "Glotrex Co Ltd";
case 0x163C:
return "intel";
case 0x163D:
return "Heidelberg Digital LLC";
case 0x163E:
return "3dpower";
case 0x163F:
return "Renishaw PLC";
case 0x1640:
return "Intelliworxx Inc";
case 0x1641:
return "MKNet Corporation";
case 0x1642:
return "Bitland";
case 0x1643:
return "Hajime Industries Ltd";
case 0x1644:
return "Western Avionics Ltd";
case 0x1645:
return "Quick-Serv. Computer Co. Ltd";
case 0x1646:
return "Nippon Systemware Co Ltd";
case 0x1647:
return "Hertz Systemtechnik GMBH";
case 0x1648:
return "MeltDown Systems LLC";
case 0x1649:
return "Jupiter Systems";
case 0x164A:
return "Aiwa Co. Ltd";
case 0x164C:
return "Department Of Defense";
case 0x164D:
return "Ishoni Networks";
case 0x164E:
return "Micrel Inc.";
case 0x164F:
return "Datavoice (Pty) Ltd.";
case 0x1650:
return "Admore Technology Inc.";
case 0x1651:
return "Chaparral Network Storage";
case 0x1652:
return "Spectrum Digital Inc.";
case 0x1653:
return "Nature Worldwide Technology Corp";
case 0x1654:
return "Sonicwall Inc";
case 0x1655:
return "Dazzle Multimedia Inc.";
case 0x1656:
return "Insyde Software Corp";
case 0x1657:
return "Brocade Communications Systems";
case 0x1658:
return "Med Associates Inc.";
case 0x1659:
return "Shiba Denshi Systems Inc.";
case 0x165A:
return "Epix Inc.";
case 0x165B:
return "Real-Time Digital Inc.";
case 0x165C:
return "Kondo Kagaku";
case 0x165D:
return "Hsing Tech. Enterprise Co. Ltd.";
case 0x165E:
return "Hyunju Computer Co. Ltd.";
case 0x165F:
return "Comartsystem Korea";
case 0x1660:
return "Network Security Technologies Inc. (NetSec)";
case 0x1661:
return "Worldspace Corp.";
case 0x1662:
return "Int Labs";
case 0x1663:
return "Elmec Inc. Ltd.";
case 0x1664:
return "Fastfame Technology Co. Ltd.";
case 0x1665:
return "Edax Inc.";
case 0x1666:
return "Norpak Corporation";
case 0x1667:
return "CoSystems Inc.";
case 0x1668:
return "Actiontec Electronics Inc.";
case 0x166A:
return "Komatsu Ltd.";
case 0x166B:
return "Supernet Inc.";
case 0x166C:
return "Shade Ltd.";
case 0x166D:
return "Sibyte Inc.";
case 0x166E:
return "Schneider Automation Inc.";
case 0x166F:
return "Televox Software Inc.";
case 0x1670:
return "Rearden Steel";
case 0x1671:
return "Atan Technology Inc.";
case 0x1672:
return "Unitec Co. Ltd.";
case 0x1673:
return "pctel";
case 0x1675:
return "Square Wave Technology";
case 0x1676:
return "Emachines Inc.";
case 0x1677:
return "Bernecker + Rainer";
case 0x1678:
return "INH Semiconductor";
case 0x1679:
return "Tokyo Electron Device Ltd.";
case 0x167F:
return "iba AG";
case 0x1680:
return "Dunti Corp.";
case 0x1681:
return "Hercules";
case 0x1682:
return "PINE Technology, Ltd.";
case 0x1688:
return "CastleNet Technology Inc.";
case 0x168A:
return "Utimaco Safeware AG";
case 0x168B:
return "Circut Assembly Corp.";
case 0x168C:
return "Atheros Communications Inc.";
case 0x168D:
return "NMI Electronics Ltd.";
case 0x168E:
return "Hyundai MultiCAV Computer Co. Ltd.";
case 0x168F:
return "KDS Innotech Corp.";
case 0x1690:
return "NetContinuum, Inc.";
case 0x1693:
return "FERMA";
case 0x1695:
return "EPoX Computer Co., Ltd.";
case 0x16AE:
return "SafeNet Inc.";
case 0x16B3:
return "CNF Mobile Solutions";
case 0x16B8:
return "Sonnet Technologies, Inc.";
case 0x16CA:
return "Cenatek Inc.";
case 0x16CB:
return "Minolta Co. Ltd.";
case 0x16CC:
return "Inari Inc.";
case 0x16D0:
return "Systemax";
case 0x16E0:
return "Third Millenium Test Solutions, Inc.";
case 0x16E5:
return "Intellon Corporation";
case 0x16EC:
return "U.S. Robotics";
case 0x16F0:
return "LaserLinc Inc.";
case 0x16F1:
return "Adicti Corp.";
case 0x16F3:
return "Jetway Information Co., Ltd";
case 0x16F6:
return "VideoTele.com Inc.";
case 0x1700:
return "Antara LLC";
case 0x1701:
return "Interactive Computer Products Inc.";
case 0x1702:
return "Internet Machines Corp.";
case 0x1703:
return "Desana Systems";
case 0x1704:
return "Clearwater Networks";
case 0x1705:
return "Digital First";
case 0x1706:
return "Pacific Broadband Communications";
case 0x1707:
return "Cogency Semiconductor Inc.";
case 0x1708:
return "Harris Corp.";
case 0x1709:
return "Zarlink Semiconductor";
case 0x170A:
return "Alpine Electronics Inc.";
case 0x170B:
return "NetOctave Inc.";
case 0x170C:
return "YottaYotta Inc.";
case 0x170D:
return "SensoMotoric Instruments GmbH";
case 0x170E:
return "San Valley Systems, Inc.";
case 0x170F:
return "Cyberdyne Inc.";
case 0x1710:
return "Pelago Networks";
case 0x1711:
return "MyName Technologies, Inc.";
case 0x1712:
return "NICE Systems Inc.";
case 0x1713:
return "TOPCON Corp.";
case 0x1725:
return "Vitesse Semiconductor";
case 0x1734:
return "Fujitsu-Siemens Computers GmbH";
case 0x1737:
return "LinkSys";
case 0x173B:
return "Altima Communications Inc.";
case 0x1743:
return "Peppercon AG";
case 0x174B:
return "PC Partner Limited";
case 0x1752:
return "Global Brands Manufacture Ltd.";
case 0x1753:
return "TeraRecon, Inc.";
case 0x1755:
return "Alchemy Semiconductor Inc.";
case 0x176A:
return "General Dynamics Canada";
case 0x1775:
return "General Electric";
case 0x1789:
return "Ennyah Technologies Corp";
case 0x1793:
return "Unitech Electronics Co., Ltd";
case 0x17A1:
return "Tascorp";
case 0x17A7:
return "Start Network Technology Co., Ltd.";
case 0x17AA:
return "Legend Ltd. (Beijing)";
case 0x17AB:
return "Phillips Components";
case 0x17AF:
return "Hightech Information Systems, Ltd.";
case 0x17BE:
return "Philips Semiconductors";
case 0x17C0:
return "Wistron Corp.";
case 0x17C4:
return "Movita";
case 0x17CC:
return "NetChip";
case 0x17cd:
return "Cadence Design Systems";
case 0x17D5:
return "Neterion Inc.";
case 0x17db:
return "Cray, Inc.";
case 0x17E9:
return "DH electronics GmbH / Sabrent";
case 0x17EE:
return "Connect Components, Ltd.";
case 0x17F3:
return "RDC Semiconductor Co., Ltd.";
case 0x17FE:
return "INPROCOMM";
case 0x1813:
return "Ambient Technologies Inc";
case 0x1814:
return "Ralink Technology, Corp.";
case 0x1815:
return "devolo AG";
case 0x1820:
return "InfiniCon Systems, Inc.";
case 0x1824:
return "Avocent";
case 0x1841:
return "Panda Platinum";
case 0x1860:
return "Primagraphics Ltd.";
case 0x186C:
return "Humusoft S.R.O";
case 0x1887:
return "Elan Digital Systems Ltd";
case 0x1888:
return "Varisys Limited";
case 0x188D:
return "Millogic Ltd.";
case 0x1890:
return "Egenera, Inc.";
case 0x18BC:
return "Info-Tek Corp.";
case 0x18C9:
return "ARVOO Engineering BV";
case 0x18CA:
return "XGI Technology Inc";
case 0x18F1:
return "Spectrum Systementwicklung Microelectronic GmbH";
case 0x18F4:
return "Napatech A/S";
case 0x18F7:
return "Commtech, Inc.";
case 0x18FB:
return "Resilience Corporation";
case 0x1904:
return "Ritmo";
case 0x1905:
return "WIS Technology, Inc.";
case 0x1910:
return "Seaway Networks";
case 0x1912:
return "Renesas Electronics";
case 0x1931:
return "Option NV";
case 0x1941:
return "Stelar";
case 0x1954 :
return "One Stop Systems, Inc.";
case 0x1969:
return "Atheros Communications";
case 0x1971:
return "AGEIA Technologies, Inc.";
case 0x197B:
return "JMicron Technology Corp.";
case 0x198a:
return "Nallatech";
case 0x1991:
return "Topstar Digital Technologies Co., Ltd.";
case 0x19a2:
return "ServerEngines";
case 0x19A8:
return "DAQDATA GmbH";
case 0x19AC:
return "Kasten Chase Applied Research";
case 0x19B6:
return "Mikrotik";
case 0x19E2:
return "Vector Informatik GmbH";
case 0x19E3:
return "DDRdrive LLC";
case 0x1A08:
return "Linux Networx";
case 0x1a41:
return "Tilera Corporation";
case 0x1A42:
return "Imaginant";
case 0x1B13:
return "Jaton Corporation USA";
case 0x1B21:
return "Asustek - ASMedia Technology Inc.";
case 0x1B6F:
return "Etron";
case 0x1B73:
return "Fresco Logic Inc.";
case 0x1B91:
return "Averna";
case 0x1BAD:
return "ReFLEX CES";
case 0x1C0F:
return "Monarch Innovative Technologies Pvt Ltd's ";
case 0x1C32:
return "Highland Technology, Inc.";
case 0x1c39:
return "Thomson Video Networks";
case 0x1DE1:
return "Tekram";
case 0x1FCF:
return "Miranda Technologies Ltd.";
case 0x2001:
return "Temporal Research Ltd";
case 0x2646:
return "Kingston Technology Co.";
case 0x270F:
return "ChainTek Computer Co. Ltd.";
case 0x2EC1:
return "Zenic Inc";
case 0x3388:
return "Hint Corp.";
case 0x3411:
return "Quantum Designs (H.K.) Inc.";
case 0x3513:
return "ARCOM Control Systems Ltd.";
case 0x38EF:
return "4links";
case 0x3D3D:
return "3Dlabs, Inc. Ltd";
case 0x4005:
return "Avance Logic Inc.";
case 0x4144:
return "Alpha Data";
case 0x416C:
return "Aladdin Knowledge Systems";
case 0x4348:
return "wch.cn";
case 0x4680:
return "UMAX Computer Corp.";
case 0x4843:
return "Hercules Computer Technology";
case 0x4943:
return "Growth Networks";
case 0x4954:
return "Integral Technologies";
case 0x4978:
return "Axil Computer Inc.";
case 0x4C48:
return "Lung Hwa Electronics";
case 0x4C53:
return "SBS-OR Industrial Computers";
case 0x4CA1:
return "Seanix Technology Inc";
case 0x4D51:
return "Mediaq Inc.";
case 0x4D54:
return "Microtechnica Co Ltd";
case 0x4DDC:
return "ILC Data Device Corp.";
case 0x4E8:
return "Samsung Windows Portable Devices";
case 0x5053:
return "TBS/Voyetra Technologies";
case 0x508A:
return "Samsung T10 MP3 Player";
case 0x5136:
return "S S Technologies";
case 0x5143:
return "Qualcomm Inc. USA";
case 0x5333:
return "S3 Graphics Co., Ltd";
case 0x544C:
return "Teralogic Inc";
case 0x5555:
return "Genroco Inc.";
case 0x5853:
return "Citrix Systems, Inc.";
case 0x6409:
return "Logitec Corp.";
case 0x6666:
return "Decision Computer International Co.";
case 0x6945:
return "ASMedia Technology Inc.";
case 0x7604:
return "O.N. Electric Co. Ltd.";
case 0x7d1:
return "D-Link Corporation";
case 0x8080:
return "Xirlink, Inc";
case 0x8086:
return "Intel Corporation";
case 0x8087:
return "Intel";
case 0x80EE:
return "Oracle Corporation - InnoTek Systemberatung GmbH";
case 0x8866:
return "T-Square Design Inc.";
case 0x8888:
return "Silicon Magic";
case 0x8E0E:
return "Computone Corporation";
case 0x9004:
return "Adaptec Inc";
case 0x9005:
return "Adaptec Inc";
case 0x919A:
return "Gigapixel Corp";
case 0x9412:
return "Holtek";
case 0x9699:
return "Omni Media Technology Inc.";
case 0x9710:
return "MosChip Semiconductor Technology";
case 0x9902:
return "StarGen, Inc.";
case 0xA0A0:
return "Aopen Inc.";
case 0xA0F1:
return "Unisys Corporation";
case 0xA200:
return "NEC Corp.";
case 0xA259:
return "Hewlett Packard";
case 0xA304:
return "Sony";
case 0xA727:
return "3com Corporation";
case 0xAA42:
return "Abekas, Inc";
case 0xAC1E:
return "Digital Receiver Technology Inc";
case 0xB1B3:
return "Shiva Europe Ltd.";
case 0xB894:
return "Brown & Sharpe Mfg. Co.";
case 0xBEEF:
return "Mindstream Computing";
case 0xC001:
return "TSI Telsys";
case 0xC0A9:
return "Micron/Crucial Technology";
case 0xC0DE:
return "Motorola";
case 0xC0FE:
return "Motion Engineering Inc.";
case 0xC622:
return "Hudson Soft Co Ltd";
case 0xCA50:
return "Varian, Inc";
case 0xCAFE:
return "Chrysalis-ITS";
case 0xCCCC:
return "Catapult Communications";
case 0xD4D4:
return "Curtiss-Wright Controls Embedded Computing";
case 0xDC93:
return "Dawicontrol";
case 0xDEAD:
return "Indigita Corporation";
case 0xDEAF:
return "Middle Digital, Inc";
case 0xE159:
return "Tiger Jet Network Inc";
case 0xE4BF:
return "EKF Elektronik GMBH";
case 0xEA01:
return "Eagle Technology";
case 0xEABB:
return "Aashima Technology B.V.";
case 0xEACE:
return "Endace Measurement Systems Ltd.";
case 0xECC0:
return "Echo Digital Audio Corporation";
case 0xEDD8:
return "ARK Logic, Inc";
case 0xF5F5:
return "F5 Networks Inc.";
case 0xFA57:
return "Interagon AS";
